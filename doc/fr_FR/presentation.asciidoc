==== Artyom, assistant vocal HTML5

Ce plugin permet d'intégrer Artyom sur le dashboard Jeedom et donc la reconnaissance vocale sur les pages web Jeedom

Attention, Artyom utilise en sous jacent des fonctions de Chrome et ne fonctionne donc que sous ce navigateur (y compris version Android)

La reconnaissance vocale est alors utilisée avec le moteur des interactions Jeedom directement et le retour lu par le plugin
